package varribas.ejercicioi;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;


public class DrawOptions extends ActionBarActivity {
    RadioGroup rgFigure;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw_options);

        rgFigure = (RadioGroup) findViewById(R.id.rgMode);
    }


    public void saveAndReturn(View view){
        int selected = rgFigure.getCheckedRadioButtonId();
        getIntent().putExtra("figure", selected);
        setResult(RESULT_OK, getIntent());

        super.finish();
    }
}
