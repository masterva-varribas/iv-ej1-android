package varribas.ejercicioi;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;


public class Main extends ActionBarActivity {
    private View lyMain;
    private Lienzo lienzo;
    private SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        lyMain = findViewById(R.id.main);
        lyMain.setBackgroundResource(R.drawable.degradado);

        lienzo = (Lienzo) findViewById(R.id.varLienzo);

        SharedPreferences preferences = getSharedPreferences("draw", Context.MODE_PRIVATE);
        editor = preferences.edit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch(id){
            //case R.id.action_settings:
            //    return true;
            case R.id.draw_options:
                Intent i = new Intent(this, DrawOptions.class);
                startActivityForResult(i, 0);
                break;
            case R.id.draw_clear:
                lienzo.clear();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK){
            Toast.makeText(this, "Fail to get response ("+resultCode+") :(", Toast.LENGTH_LONG).show();
            return;
        }

        int selected = data.getIntExtra("figure", -1);

        String mode = "circ";
        switch (selected){
            case R.id.rbCirc:
                lienzo.setBackgroundResource(R.drawable.android1);
                mode = "circ";
                break;
            case R.id.rbRect:
                lienzo.setBackgroundResource(R.drawable.android2);
                mode = "rect";
                break;
            case R.id.rbLine:
                lienzo.setBackgroundResource(R.drawable.android3);
                mode = "line";
                break;
        }
        editor.putString("draw_mode", mode);
        editor.commit();

        Toast.makeText(this, "Paint mode: "+mode, Toast.LENGTH_SHORT).show();
    }
}
