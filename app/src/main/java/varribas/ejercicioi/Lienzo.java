package varribas.ejercicioi;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Victor on 09/03/2015.
 */
public class Lienzo extends View{
    private Paint pincel, pBasic;
    private int color;

    private Bitmap bitmap;
    private Canvas bitmapWrapper;

    private float x0,y0, x1, y1;

    private SharedPreferences drawPreferences;

    /// from code >>>
    public Lienzo(Context context){
        super(context);
        onCreate();
        drawPreferences = context.getSharedPreferences("draw", Context.MODE_PRIVATE);
    }
    /// <<<

    /// from XLM >>>
    public Lienzo(Context context, AttributeSet attrs){
        super(context, attrs);
        onCreate();
        drawPreferences = context.getSharedPreferences("draw", Context.MODE_PRIVATE);
    }

    public Lienzo(Context context, AttributeSet attrs, int defStyle){
        super(context, attrs, defStyle);
        onCreate();
        drawPreferences = context.getSharedPreferences("draw", Context.MODE_PRIVATE);
    }
    /// <<<


    public void clear(){
        bitmapWrapper.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        invalidate();
    }

    private void onCreate(){
        color = Color.BLUE;

        pBasic = new Paint();
        pincel = new Paint();
        pincel.setColor(color);
        pincel.setStrokeWidth(4);
        pincel.setStyle(Paint.Style.STROKE);
    }


    //size assigned to view
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmapWrapper = new Canvas(bitmap);
    }


    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);
        canvas.drawBitmap(bitmap, 0,0, pBasic);

    }

    public boolean onTouchEvent(MotionEvent event){
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                x0 = event.getX();
                y0 = event.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:
                x1 = event.getX();
                y1 = event.getY();
                drawIt();
                break;
        }

        this.invalidate(); // se fuerza el repintado
        //return super.onTouchEvent(event);
        return true;
    }


    private void drawIt(){
        String draw_mode = drawPreferences.getString("draw_mode", "circ");
        Log.d("DRAW IT", draw_mode);
        switch(draw_mode) {
            case "circ":
                bitmapWrapper.drawCircle(x0, y0, Math.max(Math.abs(x1 - x0), Math.abs(y1 - y0)), pincel);
                break;
            case "rect":
                bitmapWrapper.drawRect(x0, y0, x1, y1, pincel);
                break;
            case "line":
                bitmapWrapper.drawLine(x0, y0, x1, y1, pincel);
                break;
        }
    }
}
